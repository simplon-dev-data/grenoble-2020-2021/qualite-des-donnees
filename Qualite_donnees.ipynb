{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Activité sur la qualité des données\n",
    "\n",
    "<img src=\"https://upload.wikimedia.org/wikipedia/commons/thumb/7/75/Open_Food_Facts_logo.svg/1200px-Open_Food_Facts_logo.svg.png\" width=\"400\">\n",
    "\n",
    "Dans cette activité, on va travailler avec la base de donnée d'OpenFoodFacts (https://fr.openfoodfacts.org/). Les données sont stockées dans une base de données No-SQL MongoDB. Dans ce type de base de données orientée document, les données sont stockées sous forme de JSON binaires (BSON). Pour cette activité, on utilisera un fichier CSV (`off_data.csv`) de 1000 lignes et 11 colonnes issu de l'extraction de la base de données.\n",
    "\n",
    "Dans cette activité, on cherchera d'abord à estimer la qualité du jeu de données puis on cherchera à l'améliorer. L'activité sera réalisée **en parallèle** en Python et en SQL (SQLite et/ou PostgreSQL).\n",
    "\n",
    "Dans un contexte de datascience, cette activité sur l'analyse de la qualité des données fait partie de l'**analyse exploratoire des données (Exploratory Data Analysis - EDA)**. \n",
    "\n",
    "## Mise en place de l'activité"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A l'aide de **pipenv**, installer les bibliothèques **jupyter, numpy, pandas, seaborn, psycopg2-binary et sqlalchemy** dans un environnement virtuel."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Import des librairies Python\n",
    "import numpy as np\n",
    "import pandas as pd\n",
    "import seaborn as sns"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Connexion pour SQlite\n",
    "import sqlite3\n",
    "\n",
    "conn_sqlite = sqlite3.connect('my_data.db')\n",
    "data = pd.read_csv(\"off_data.csv\")\n",
    "data.to_sql('data', conn_sqlite, if_exists='replace', index = False)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 25,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Connexion pour PostgreSQL\n",
    "import psycopg2\n",
    "from psycopg2.extensions import parse_dsn\n",
    "from sqlalchemy import create_engine\n",
    "\n",
    "db_dsn = \"postgres://postgres:test@localhost:5432/decouverte\"\n",
    "db_args = parse_dsn(db_dsn)\n",
    "conn_postgre = psycopg2.connect(**db_args)\n",
    "data = pd.read_csv(\"off_data.csv\")\n",
    "engine = create_engine(db_dsn)\n",
    "data.to_sql('data', engine, if_exists='replace', index = False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Analyse du jeu de données\n",
    "\n",
    "> Les données manquantes sont représentées sous différentes formes suivant le langage de données utilisé. En SQL, les données manquantes sont représentées par **NULL**. En Python, on utilise **None**. Dans les bibliothèques de datascience en Python, on utilise **Nan** (pour not a number)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> La bibliothèque pandas possède des fonctions pour importer des données de différentes sources. La liste exhaustive se trouve à l'adresse suivante : https://pandas.pydata.org/pandas-docs/stable/reference/io.html. Pour cette activité, on va utiliser un fichier CSV donc on utilisera la fonction `pd.read_csv()`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**ACTIVITE 1**\n",
    "\n",
    "- Créer une variable `df` contenant le dataframe pandas avec les valeurs du fichier CSV\n",
    "\n",
    "- Afficher un résumé des informations sur les données du dataframe à l'aide d'une méthode de dataframe (nombre de lignes, nombres de valeurs présentes par colonne, types de valeurs...)\n",
    "\n",
    "- A l'aide d'un requête SQL (en SQLite et/ou PostgreSQL), vérifier les valeurs du nombre total de lignes dans le fichier CSV ainsi que le nombre de valeurs manquantes dans 2 colonnes du jeu de données\n",
    "\n",
    "- En Python et en SQL, calculer le pourcentage de valeurs manquantes dans chacune des colonnes du jeu de données\n",
    "\n",
    "- En Python et en SQL, créer un nouveau dataframe (ou nouvelle table en SQL) qui ne contient que les colonnes qui ont moins de 10% de valeurs manquantes\n",
    "\n",
    "- A partir de la question précédente, créer une fonction Python `clean_data()` qui crée le dataframe (ou la table) contenant uniquement les colonnes avec le moins de données manquantes :\n",
    "\n",
    "```python\n",
    "# Utilisation avec pandas\n",
    "df_clean = clean_data(data_type=\"pandas\", df=df, max_missing=10)\n",
    "# Utilisation en SQL\n",
    "clean_data(data_type=\"sql\", conn=conn, table_name=\"data\", max_missing=10)\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> En plus de connaitre le nombre de valeurs manquantes, il est aussi utile de connaître leur répartition dans le jeu de données. Si trop de valeurs sont manquantes pour une même ligne, la donnée risque d'être difficilement exploitable. On va analyser la répartition des données manquantes de deux façons : une **graphique** et une **statistique**."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**ACTIVITE 2**\n",
    "\n",
    "- A l'aide de la fonction `heatmap()` de seaborn et de la méthode de dataframe `isnull()`, afficher la répartition des valeurs manquantes dans le jeu de données\n",
    "\n",
    "- En Python, calculer le nombre de valeurs manquantes par ligne"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> Pour connaître la qualité des données pour les colonnes contenant des catégories, on peut regarder les différentes valeurs contenues dans chacune de ces colonnes. La méthode de `pandas.Series` nommée `value_counts()` est très utile dans ce cas (à partir de la version 1.1.0 de pandas on peut l'utiliser aussi sur les `pandas.DataFrame`)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**ACTIVITE 3**\n",
    "\n",
    "- Pour les colonnes de catégories, afficher le nombre de valeurs par catégorie à l'aide de pandas\n",
    "\n",
    "On s'intéresse maintenant uniquement à la colonne *pnns_groups_1*\n",
    "\n",
    "- Afficher le nombres de valeurs pour chacune des catégories de cette colonne\n",
    "\n",
    "- Vérifier en SQL les valeurs trouvées à la question précédente "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> La colonne *pnns_group_1* contient des valeurs manquantes \"cachées\". Pour améliorer la qualité des données, il faut transformer ces données en valeurs manquantes. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**ACTIVITE 4**\n",
    "\n",
    "- En python, transformer toutes les valeurs *unknown* en valeurs manquantes (nan)\n",
    "\n",
    "- Même chose en SQL\n",
    "\n",
    "- En Python et en SQL, vérifier le résultat\n",
    "\n",
    "- En python, transformer directement les valeurs *unknown* de la colonne *pnns_groups_1* en valeurs manquantes à partir de la méthode `read_csv()`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> Dans la colonne *pnns_group_1*, les catégories *Sugary snacks* et *sugary-snacks* correspondent à la même chose. Pour améliorer la qualité du jeu de données, il faut regrouper ces deux catégories."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**ACTIVITE 5**\n",
    "\n",
    "- En Python et en SQL, regrouper les catégories *sugary-snacks* et *Sugary snacks* en une seule\n",
    "\n",
    "- En Python et en SQL, vérifier le résultat"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> Pour améliorer la qualité d'un jeu de données avec des données manquantes, on peut soit supprimer les données (colonnes ou lignes) soit les remplacer par d'autres valeurs (imputation) "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**ACTIVITE 6**\n",
    "\n",
    "- Créer un dataframe qui ne contient que les colonnes avec moins de 30% de données manquantes\n",
    "\n",
    "- Supprimer la colonne *pnns_groups_1* qui contient plus de 30% de données manqantes\n",
    "\n",
    "- Remplacer les valeurs manquantes de la colonne *countries* par le mode de la colonne. Existe-t-il une imputation plus intéressante ?\n",
    "\n",
    "- Remplacer les valeurs manquantes de la colonne *energy_100g* par la moyenne de la colonne. Existe-t-il une imputation plus intéressante ?\n",
    "\n",
    "- Supprimer les lignes dont la colonne *product_name* est vide\n",
    "\n",
    "- Vérifier qu'il n'y a plus de données manquantes"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
